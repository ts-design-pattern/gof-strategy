/**
 * 專案名稱： gof-strategy
 * 檔案說明： 策略模式範例程式進入點
 * -----------------------------------------------------------------------------
 * @NOTE
 */

import { Customer } from './customer';
import { Cash, CreditCard, Transfer } from './shared';

// 客戶 A 使用信用卡付款
const creditCard = new CreditCard();
const customerA = new Customer('A');
customerA.pay(1000, creditCard);

// 客戶 A 使用現金付款
const cash = new Cash();
const custumerB = new Customer('B');
custumerB.pay(110, cash);

// 客戶 C 使用轉帳付款
const transfer = new Transfer();
const custumerC = new Customer('C');
custumerC.pay(10000, transfer);
