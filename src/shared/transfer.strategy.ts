/**
 * 專案名稱： gof-strategy
 * 檔案說明： 轉帳付款方式策略
 * -----------------------------------------------------------------------------
 * @NOTE
 */

import { Payment } from './../core';

/**
 * 轉帳付款方式策略
 */
export class Transfer implements Payment {
  /**
   * 付款
   *
   * @method public
   * @param amount 付款金額
   */
  public pay(amount: number): void {
    console.log(`pay ${amount} by transfer`);
  }
}
