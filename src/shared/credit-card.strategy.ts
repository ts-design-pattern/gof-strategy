/**
 * 專案名稱： gof-strategy
 * 檔案說明： 信用卡付款方式策略
 * -----------------------------------------------------------------------------
 * @NOTE
 */

import { Payment } from './../core';

/**
 * 信用卡付款方式策略
 */
export class CreditCard implements Payment {
  /**
   * 付款
   *
   * @method public
   * @param amount 付款金額
   */
  public pay(amount: number): void {
    console.log(`pay ${amount} by credit card`);
  }
}
