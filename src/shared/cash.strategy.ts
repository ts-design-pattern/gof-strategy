/**
 * 專案名稱： gof-strategy
 * 檔案說明： 現金付款方式策略
 * -----------------------------------------------------------------------------
 * @NOTE
 */

import { Payment } from './../core';

/**
 * 現金付款方式策略
 */
export class Cash implements Payment {
  /**
   * 付款
   *
   * @method public
   * @param amount 付款金額
   */
  public pay(amount: number): void {
    console.log(`pay ${amount} by cash`);
  }
}
