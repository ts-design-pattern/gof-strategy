/**
 * 專案名稱： gof-strategy
 * 檔案說明： 客戶
 * -----------------------------------------------------------------------------
 * @NOTE
 */

import { Payment } from './core';

/**
 * 客戶
 */
export class Customer {
  /**
   * @param name 客戶名稱
   */
  constructor(protected name: string) {}

  /**
   * 付款
   *
   * @method public
   * @param amount  付款金額
   * @param payment 付款方式
   */
  public pay(amount: number, payment: Payment): void {
    console.log(`customer ${this.name}`);
    payment.pay(amount);
    console.log('\n');
  }
}
