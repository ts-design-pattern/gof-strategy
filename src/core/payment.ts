/**
 * 專案名稱： gof-strategy
 * 檔案說明： 抽象付款方式策略
 * -----------------------------------------------------------------------------
 * @NOTE
 */

/**
 * 抽象付款方式策略
 */
export interface Payment {
  /**
   * 付款
   *
   * @method public
   * @param amount 付款金額
   */
  pay(amount: number): void;
}
