/**
 * 專案名稱： gof-strategy
 * 檔案說明： 產量 API
 * -----------------------------------------------------------------------------
 * @NOTE
 */

/**
 * 產量 API
 */
export class YieldApi {
  /**
   * 查詢產量
   *
   * @method public
   * @param query 查詢語句
   * @return 回傳產量數據
   */
  public search(query: object): string {
    return `${JSON.stringify(query)}\n`;
  }
}
