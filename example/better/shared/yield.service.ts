/**
 * 專案名稱： gof-strategy
 * 檔案說明： 產量數據查詢服務
 * -----------------------------------------------------------------------------
 * @NOTE
 */

import { QueryStrategy } from '../core';
import { YieldApi } from './yield.api';

/**
 * 產量數據查詢服務
 */
export class YieldService {
  /**
   * @param api 產量 API
   */
  constructor(protected api = new YieldApi()) {}

  /**
   * 查詢特定時間、特定 Site 的總產量
   *
   * @method public
   * @param start 開始時間
   * @param end   結束時間
   * @param site  Site
   * @return 回傳特定時間、特定 Site 的總產量
   */
  public search(query: QueryStrategy): string {
    return this.api.search(query.generate().toJSON());
  }
}
