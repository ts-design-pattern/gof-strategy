/**
 * 專案名稱： gof-strategy
 * 檔案說明： 特定時間、特定 Site 的總產量查詢語句策略
 * -----------------------------------------------------------------------------
 * @NOTE
 */

import * as esb from 'elastic-builder';
import { QueryStrategy } from '../core';

/**
 * 特定時間、特定 Site 的總產量查詢語句策略
 */
export class SpecificSiteQuery implements QueryStrategy {
  /**
   * @param start 開始時間
   * @param end   結束時間
   * @param site  Site
   */
  constructor(
    protected start: number,
    protected end: number,
    protected site: string,
  ) {}

  /**
   * 產生查詢語句
   *
   * @method public
   * @return 回傳查詢語句
   */
  public generate(): esb.RequestBodySearch {
    return esb
      .requestBodySearch()
      .query(
        esb
          .boolQuery()
          .must([
            esb.rangeQuery('evt_dt').gte(this.start).lt(this.end),
            esb.termQuery('site', this.site),
          ]),
      )
      .agg(esb.sumAggregation('count', 'count'));
  }
}
