/**
 * 專案名稱： gof-strategy
 * 檔案說明： 抽象查詢語句建構策略
 * -----------------------------------------------------------------------------
 * @NOTE
 */

import * as esb from 'elastic-builder';

/**
 * 抽象查詢語句建構策略
 */
export interface QueryStrategy {
  /**
   * 產生查詢語句
   *
   * @method public
   * @return 回傳查詢語句
   */
  generate(): esb.RequestBodySearch;
}
