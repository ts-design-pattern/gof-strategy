/**
 * 專案名稱： gof-strategy
 * 檔案說明： 較佳的開發模式程式進入點
 * -----------------------------------------------------------------------------
 * @NOTE
 */

import {
  DailySpecificPlantcodeQuery,
  SpecificPlantcodeEachLineQuery,
  SpecificSiteQuery,
  YieldService,
} from './shared';

const yieldService = new YieldService();
const start = 1630425600000;
const end = 1632931200000;

// Site 層級
const siteStrategy = new SpecificSiteQuery(start, end, 'WKS');
console.log(yieldService.search(siteStrategy));

// Plant 一級
const plantStrategy1 = new DailySpecificPlantcodeQuery(start, end, 'F232');
console.log(yieldService.search(plantStrategy1));

// Plant 二級
const plantStrategy2 = new SpecificPlantcodeEachLineQuery(start, end, 'F232');
console.log(yieldService.search(plantStrategy2));
