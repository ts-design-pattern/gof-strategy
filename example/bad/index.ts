/**
 * 專案名稱： gof-strategy
 * 檔案說明： 不好的範例程式進入點
 * -----------------------------------------------------------------------------
 * @NOTE
 */

import { YieldService } from './yield.service';

const yieldService = new YieldService();
const start = 1630425600000;
const end = 1632931200000;

// Site 層級
console.log(yieldService.fetchSpecificSite(start, end, 'WKS'));

// Plant 一級
console.log(yieldService.fetchDailySpecificPlantCode(start, end, 'F232'));

// Plant 二級
console.log(yieldService.fetchSpecificPlantCodeEachLine(start, end, 'F232'));
