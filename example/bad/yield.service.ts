/**
 * 專案名稱： gof-strategy
 * 檔案說明： 產量數據查詢服務
 * -----------------------------------------------------------------------------
 * @NOTE
 */

import * as esb from 'elastic-builder';
import { YieldApi } from './yield.api';

/**
 * 產量數據查詢服務
 */
export class YieldService {
  /**
   * @param api 產量 API
   */
  constructor(protected api = new YieldApi()) {}

  /**
   * 查詢特定時間、特定 Site 的總產量
   *
   * @method public
   * @param start 開始時間
   * @param end   結束時間
   * @param site  Site
   * @return 回傳特定時間、特定 Site 的總產量
   */
  public fetchSpecificSite(start: number, end: number, site: string): string {
    const query = esb
      .requestBodySearch()
      .query(
        esb
          .boolQuery()
          .must([
            esb.rangeQuery('evt_dt').gte(start).lt(end),
            esb.termQuery('site', site),
          ]),
      )
      .agg(esb.sumAggregation('count', 'count'));
    return this.api.search(query.toJSON());
  }

  /**
   * 查詢特定時間、特定廠別的總產量、並以 Daily 呈現總產量
   *
   * @method public
   * @param start     開始時間
   * @param end       結束時間
   * @param plantCode 廠別代碼
   * @return 回傳特定時間、特定廠別的總產量、並以 Daily 呈現總產量
   */
  public fetchDailySpecificPlantCode(
    start: number,
    end: number,
    plantCode: string,
  ): string {
    const query = esb
      .requestBodySearch()
      .query(
        esb
          .boolQuery()
          .must([
            esb.rangeQuery('evt_dt').gte(start).lt(end),
            esb.termQuery('plantCode', plantCode),
          ]),
      )
      .agg(
        esb
          .dateHistogramAggregation('date', 'evt_dt', '1d')
          .timeZone('+08:00')
          .agg(esb.sumAggregation('count', 'count')),
      );
    return this.api.search(query.toJSON());
  }

  /**
   * 查詢特定時間、特定廠別的總產量，各線別的總產量
   *
   * @method public
   * @param start     開始時間
   * @param end       結束時間
   * @param plantCode 廠別代碼
   * @return 回傳特定時間、特定廠別的總產量，各線別的總產量
   */
  public fetchSpecificPlantCodeEachLine(
    start: number,
    end: number,
    plantCode: string,
  ): string {
    const query = esb
      .requestBodySearch()
      .query(
        esb
          .boolQuery()
          .must([
            esb.rangeQuery('evt_dt').gte(start).lt(end),
            esb.termQuery('plantCode', plantCode),
          ]),
      )
      .agg(
        esb
          .termsAggregation('line', 'line')
          .agg(esb.sumAggregation('count', 'count')),
      );
    return this.api.search(query.toJSON());
  }
}
